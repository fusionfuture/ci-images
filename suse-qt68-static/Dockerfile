# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: Volker Krause <vkrause@kde.org>

FROM opensuse/tumbleweed
MAINTAINER Volker Krause <vkrause@kde.org>

# Update container
RUN zypper --non-interactive --gpg-auto-import-keys -v dup
# CUPS-devel at least does not like Busybox versions of some things, so ensure it is not used
# Likewise, we have packages that do not want LibreSSL so lock it out too
RUN zypper al busybox busybox-gzip libressl-devel
# Install various other packages
RUN zypper --non-interactive install python3-lxml python3-paramiko python3-PyYAML python3-simplejson wget file tar gzip rsync
# ffmpegthumbs, needs to be before devel_qt or the ffmpeg-6-mini-libs package will be installed with no codec at all
RUN zypper --non-interactive install ffmpeg-6-libavcodec-devel ffmpeg-6-libavfilter-devel ffmpeg-6-libavformat-devel ffmpeg-6-libavdevice-devel ffmpeg-6-libavutil-devel ffmpeg-6-libswscale-devel ffmpeg-6-libpostproc-devel

# Prevent Qt from being installed by packages in any way
RUN zypper addlock qt5* qt6* libQt5* libQt6*

# Install build dependencies
RUN zypper --non-interactive install --recommends -t pattern devel_C_C++
# And some other useful and base packages
RUN zypper --non-interactive in \
    # SCM utils
    git git-lfs \
    # Additional compilers used for various bits of integration (clazy, kdevelop)
    # ccache is used to speed up builds as well for large projects
    clang ccache \
    # Additional linker
    mold \
    # Additional build systems (non-cmake)
    ninja meson \
    # Pip: needed to install various Python modules
    python3-pip \
    # Python bindings for accessibility automation
    python3-atspi \
    # Sphinx documentation tooling for ECM docs
    python3-Sphinx \
    # Ruby for various places
    ruby-devel libffi-devel \
    # Utilities to bring up a headless X server instance
    xvfb-run openbox \
    # Utilities to validate Appstream metadata
    AppStream \
    # For building documentation tarballs
    bzip2 \
    # For image thumbnails for the KDE.org/applications subsite
    ImageMagick \
    # Needed for API Documentation generation
    python3-gv graphviz-gd doxygen \
    # Needed for some unit tests to function correctly
    hicolor-icon-theme \
    # Needed for some projects that use the non-standard catch2 Unittest mechanisms.
    Catch2-devel \
    # Needed by KDE Connect on X11
    libfakekey-devel

# Use mold as the default linker, as it is magnitudes faster than ld.bfd
RUN /usr/sbin/update-alternatives --set ld /usr/bin/ld.mold

# Install components needed for the CI tooling to operate (python-gitlab, gcovr, cppcheck_codequality) as well as other CI jobs (check-jsonschema)
# as well as reuse (for unit tests), and cheroot/wsgidav/ftpd (for KIO unit tests)
RUN pip install --break-system-packages python-gitlab gcovr cheroot wsgidav
RUN gem install ftpd

# KDE stuff also depends on the following
RUN zypper --non-interactive in \
    # modemmanager-qt
    ModemManager-devel \
    # networkmanager-qt
    NetworkManager-devel \
    # kcoreaddons
    lsof \
    # kauth
    polkit-devel \
    # kwindowsystem
    xcb-*-devel \
    # karchive
    libzstd-devel \
    zstd \
    # ki18n
    glibc-locale \
    # prison
    libdmtx-devel qrencode-devel \
    # kimageformats
    openexr-devel libavif-devel libheif-devel libraw-devel jxrlib-devel libjxl-devel \
    # kwayland and kwin
    wayland-devel \
    wayland-protocols-devel \
    libdisplay-info-devel \
    libei-devel \
    # baloo/kfilemetadata (some for okular)
    libattr-devel libexiv2-devel libtag-devel libtag-devel libepub-devel libpoppler-devel lmdb-devel \
    # kdoctools
    perl-URI docbook_4 docbook-xsl-stylesheets libxml2-devel libxslt-devel perl-URI \
    # kio
    libacl-devel libmount-devel libblkid-devel \
    # various projects need OpenSSL
    libopenssl-devel \
    # kdnssd
    libavahi-devel libavahi-glib-devel libavahi-gobject-devel \
    # khelpcenter and pim
    libxapian-devel \
    # sonnet
    aspell \
    aspell-devel \
    hunspell-devel \
    libvoikko-devel \
    # kio-extras and krdc, kio-fuse
    libssh-devel fuse3-devel libseccomp-devel djvulibre ms-gsl-devel \
    # plasma-pa
    libpulse-devel libcanberra-devel pipewire-pulseaudio \
    # user-manager
    libpwquality-devel \
    # sddm-kcm
    libXcursor-devel \
    # plasma-workspace
    libappindicator3-devel \
    libXtst-devel \
    umockdev-devel \
    xdotool \
    # plasma-desktop
    xf86-input-synaptics-devel xf86-input-evdev-devel xf86-input-libinput-devel libxkbfile-devel libxkbregistry-devel xorg-x11-server-sdk xdg-user-dirs shared-mime-info \
    # kimpanel
    ibus-devel scim-devel \
    # pim
    libical-devel \
    # <misc>
    alsa-devel fftw3-devel adobe-sourcecodepro-fonts \
    # kgamma5
    libXxf86vm-devel \
    # drkonqi
    at-spi2-core which libgirepository-1_0-1 typelib-1_0-Atspi-2_0 gobject-introspection-devel \
    # kcalc
    mpfr-devel \
    mpc-devel \
    # kdevelop
    gdb \
    libstdc++6-pp \
    # labplot
    gsl-devel liblz4-devel libcerf-devel hdf5-devel netcdf-devel libmatio-devel liborcus-devel \
    # wacomtablet
    libwacom-devel \
    xf86-input-wacom-devel \
    # rust-qt-binding-generator
    rust rust-std \
    cargo \
    # kdevelop
    clang \
    clang-devel \
    llvm-devel \
    subversion-devel \
    python3-devel \
    # ark
    libarchive-devel libzip-tools libzip-devel \
    # k3b
    flac-devel \
    libmad-devel \
    libmp3lame-devel \
    libogg-devel libvorbis-devel \
    libsamplerate-devel \
    # kamera
    libgphoto2-devel \
    # krfb
    LibVNCServer-devel \
    # kscd
    libdiscid-devel \
    # kajongg
    python3-Twisted \
    # okular
    texlive-latex libdjvulibre-devel libmarkdown-devel chmlib-devel \
    # ksmtp tests
    cyrus-sasl-plain \
    # Gwenview
    cfitsio-devel \
    # Calligra, Krita and probably other things elsewhere too
    libboost_*-devel \
    # Amarok
    gmock gtest libcurl-devel libofa-devel libgpod-devel libmtp-devel loudmouth-devel \
    libmariadbd-devel \
    # liblastfm-qt5-devel TODO not available for qt6 yet
    # Cantor
    libspectre-devel \
    python3-numpy \
    python3-matplotlib \
    libqalculate-devel \
    # KPat
    freecell-solver-devel black-hole-solver-devel \
    # Keysmith
    libsodium-devel \
    # Plasma Phone Components
    libphonenumber-devel \
    # kquickcharts
    glslang-devel \
    # xdg-desktop-portal-kde
    pipewire pipewire-devel \
    # kitinerary, qrca
    zxing-cpp-devel \
    # ki18n
    iso-codes-devel \
    iso-codes-lang \
    # plasma-pass
    liboath-devel \
    # Krita
    Vc-devel libmypaint-devel libheif-devel openjpeg2-devel \
    # kup
    libgit2-devel \
    # plasma-nm
    mobile-broadband-provider-info \
    # Spacebar
    c-ares-devel \
    # kxstitch
    ImageMagick-devel \
    libMagick++-devel \
    # plasma-dialer (kde-telephony-daemon)
    callaudiod-devel \
    # discover, flatpak-kcm
    flatpak-devel \
    # xdg-portal-test-kde
    gstreamer-devel \
    gstreamermm-devel \
    # Haruna
    mpv-devel \
    # kscreenlocker
    libpamtest-devel \
    # NeoChat
    olm-devel \
    # Marble
    libshp-devel \
    # KRdc
    freerdp2-devel winpr2-devel \
    # Glaxnimate
    potrace-devel \
    # kinfocenter Appium test using selenium-webdriver-at-spi
    wayland-utils \
    # marknote
    md4c-devel \
    # kinfocenter
    pciutils-devel \
    # qtbase
    libxkbcommon-x11-devel \
    libxcb-devel \
    # konvex
    assimp-devel

## build static Qt
ARG QT_VERSION=6.8.0
ARG QT_GIT_URL="https://invent.kde.org/qt"

RUN cd && git clone --branch v${QT_VERSION} --depth 1 ${QT_GIT_URL}/qt/qtbase.git \
    && cd qtbase \
    && ./configure -prefix /usr -opensource -confirm-license -nomake tests -nomake examples -static \
    && cat config.summary \
    && cmake --build . --parallel `nproc` \
    && cmake --install . \
    && rm -rf ~/qtbase

RUN cd && git clone --branch v${QT_VERSION} --depth 1 ${QT_GIT_URL}/qt/qtshadertools.git \
    && cd qtshadertools \
    && cmake . -DCMAKE_INSTALL_PREFIX=/usr \
    && cmake --build . --parallel `nproc` \
    && cmake --install . \
    && rm -rf ~/qtshadertools

RUN cd && git clone --branch v${QT_VERSION} --depth 1 ${QT_GIT_URL}/qt/qtsvg.git \
    && cd qtsvg \
    && cmake . -DCMAKE_INSTALL_PREFIX=/usr \
    && cmake --build . --parallel `nproc` \
    && cmake --install . \
    && rm -rf ~/qtsvg

RUN cd && git clone --branch v${QT_VERSION} --depth 1 ${QT_GIT_URL}/qt/qtdeclarative.git \
    && cd qtdeclarative \
    && cmake . -DCMAKE_INSTALL_PREFIX=/usr \
    && cmake --build . --parallel `nproc` \
    && cmake --install . \
    && rm -rf ~/qtdeclarative

RUN cd && git clone --branch v${QT_VERSION} --depth 1 ${QT_GIT_URL}/qt/qtwayland.git \
    && cd qtwayland \
    && cmake . -DCMAKE_INSTALL_PREFIX=/usr \
    && cmake --build . --parallel `nproc` \
    && cmake --install . \
    && rm -rf ~/qtwayland

RUN cd && git clone --branch v${QT_VERSION} --depth 1 ${QT_GIT_URL}/qt/qttools.git \
    && cd qttools \
    && cmake . -DCMAKE_INSTALL_PREFIX=/usr -DFEATURE_assistant=OFF -DFEATURE_designer=OFF \
    && cmake --build . --parallel `nproc` \
    && cmake --install . \
    && rm -rf ~/qttools

RUN cd && git clone --branch v${QT_VERSION} --depth 1 ${QT_GIT_URL}/qt/qtmultimedia.git \
    && cd qtmultimedia \
    && cmake . -DCMAKE_INSTALL_PREFIX=/usr -DFEATURE_ffmpeg=OFF \
    && cmake --build . --parallel `nproc` \
    && cmake --install . \
    && rm -rf ~/qtmultimedia

RUN cd && git clone --branch v${QT_VERSION} --depth 1 ${QT_GIT_URL}/qt/qtspeech.git \
    && cd qtspeech \
    && cmake . -DCMAKE_INSTALL_PREFIX=/usr \
    && cmake --build . --parallel `nproc` \
    && cmake --install . \
    && rm -rf ~/qtspeech

# For D-Bus to be willing to start it needs a Machine ID
RUN dbus-uuidgen > /etc/machine-id
# Certain X11 based software is very particular about permissions and ownership around /tmp/.X11-unix/ so ensure this is right
RUN mkdir /tmp/.X11-unix/ && chown root:root /tmp/.X11-unix/ && chmod 1777 /tmp/.X11-unix/
# We need a user account to do things as, and we need specific group memberships to be able to access video/render DRM nodes
RUN groupadd -g 44 host-video && groupadd -g 109 host-render && useradd -d /home/user/ -u 1000 --user-group --create-home -G video,host-video,host-render --shell /usr/bin/bash user

# Switch to our unprivileged user account
USER user
