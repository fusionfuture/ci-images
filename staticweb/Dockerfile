FROM ubuntu:24.04

LABEL Description="KDE Static website generation image"
MAINTAINER Bhushan Shah

USER root

ENV LANG en_US.UTF-8
ENV LC_ALL en_US.UTF-8

ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get install --no-install-recommends --yes \
ruby ruby-dev \
build-essential \
libsqlite3-dev \
zlib1g-dev \
# git to get stuff
git git-lfs \
# Python is used below to for a bunch of packages as well as for some generation scripts for our hugo websites, Sphinx etc.
python3-pip \
python3-venv \
# fontforge is used by breeze-icons to generate the web icon font
fontforge \
locales \
# hugo is go based
golang-go \
# dependencies for api.kde.org generation
doxygen graphviz qdoc-qt5 \
# Our Sphinx sites produce lots of duplicates for the different languages
# We use rdfind to eliminate duplicates and symlinks to make absolute symlinks relative
rdfind symlinks \
wget curl rsync pandoc subversion gettext \
# GCompris website generation:
# - Qt5 tools for lconvert and lrelease
# - htmlmin, PyQt5 and Jinja2 as Python dependencies
qttools5-dev-tools python3-pyqt5 python3-pyqt5.qtquick \
# gnupg is needed for apt-key
gnupg \
&& apt-get clean

RUN locale-gen en_US.UTF-8

# Node.js and Yarn: aether-sass theme, 25for25.kde.org, breeze webfont
RUN curl -fsSL https://deb.nodesource.com/setup_20.x | bash -
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - && echo "deb https://dl.yarnpkg.com/debian/ stable main" > /etc/apt/sources.list.d/yarn.list
RUN apt-get update && apt-get install --yes yarn nodejs && apt-get clean

# Hugo needed by many of our static sites
RUN wget https://github.com/gohugoio/hugo/releases/download/v0.134.1/hugo_extended_0.134.1_linux-amd64.deb && \
    dpkg -i hugo_extended_0.134.1_linux-amd64.deb && rm hugo_extended_0.134.1_linux-amd64.deb

RUN gem install \
    xdg:2.2.5 \
    # Jeykyll is still used by a few sites
    jekyll:3.8.4 jekyll-feed jekyll-theme-minimal jekyll-readme-index jekyll-relative-links jekyll-environment-variables jekyll-kde-theme \
    # what do we need the following stuff for?
    sqlite3:1.7.3 bundler haml redcarpet minima rdiscount inqlude addressable rouge

# We are aware that installing packages through pip next to apt managed python packages can have side effects
RUN dpkg-divert --rename --add /usr/lib/$(py3versions -d)/EXTERNALLY-MANAGED

RUN pip3 install \
    pytx dateparser jieba \
    # somehow jekyll related stuff: what do we need this excatly for?
    python-frontmatter \
    # feedparser is used by planet.kde.org
    feedparser \
    # rolisteam.org and wiki.rolisteam.org
    pelican beautifulsoup4 mdx_video \
    # Pelican has a number of plugins which Rolisteam is using that rely on functionality deprecated in Python Markdown 3.2 and which was ripped out in 3.4
    # Until they fix this, lock ourselves to 3.3 to ensure we have a version installed those plugins can work with. See https://phabricator.kde.org/T15881
    markdown==3.3 \
    # apps.kde.org generation
    py-appstream python-gitlab polib requests \
    # Build Notifier
    matrix-nio \
    # CI Tooling
    lxml \
    # CI Tooling, hugo-i18n and apps.kde.org
    pyyaml \
    # Paramiko to support interacting with the CI Notary Service
    paramiko \
    # Sphinx iteself and themes and extensions
    sphinx==7.2.6 sphinx-intl aether-sphinx sphinx_rtd_theme sphinxcontrib-doxylink sphinxcontrib-websupport \
    # Sphinx extensions for docs.kdenlive.org
    sphinxcontrib-video \
    # Sphinx extensions for docs.labplot.org
    sphinxcontrib-youtube breathe \
    # GCompris website generation
    htmlmin Jinja2

# Translation framework for the Hugo sites
RUN git clone https://invent.kde.org/websites/hugo-i18n && pip3 install ./hugo-i18n

# Automated HTML tests used by many of our generated / static websites
RUN curl https://htmltest.wjdp.uk | bash -s -- -b /usr/local/bin

# Generation of api docs for api.kde.org
RUN git clone https://invent.kde.org/frameworks/kapidox.git /kapidox \
    && pip3 install --no-deps -r /kapidox/requirements.frozen.txt /kapidox

# Setup a user account for everything else to be done under, cleanup the account Ubuntu gives us as well
RUN userdel ubuntu && rm -rf /home/ubuntu/
RUN useradd -d /home/user/ -u 1000 --user-group --create-home -G video user

# Switch to our unprivileged user account
USER user
